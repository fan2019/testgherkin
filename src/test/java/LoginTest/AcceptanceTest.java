package LoginTest;

import FileAction.CurrentPath;
import net.serenitybdd.jbehave.SerenityStories;
import net.thucydides.core.reports.OutcomeFormat;
import net.thucydides.core.reports.TestOutcomeLoader;
import net.thucydides.core.reports.TestOutcomes;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import java.io.*;

public class AcceptanceTest extends SerenityStories {

    private static String hardPath = CurrentPath.getTheHardPath();

   public AcceptanceTest() throws Throwable{

       // get the purpose file resource
       Resource newRes = new ClassPathResource("stories/newStory.story");
       File newFile = newRes.getFile();
       // refactor the original story
       Refactor.writeStory(newFile);

       runSerenity().inASingleSession();

       getCSVReport();

       identity();

   }

    public static void getCSVReport(){

        CSVReporter csvReporter = new CSVReporter(new File(hardPath+"target/surefire-reports/"));
        OutcomeFormat format = OutcomeFormat.JSON;
        TestOutcomeLoader.TestOutcomeLoaderBuilder builder = TestOutcomeLoader.loadTestOutcomes().inFormat(format);
        try {
            TestOutcomes outcomes = builder.from(new File(hardPath+"target/site/serenity/"));
            csvReporter.generateReportFor(outcomes,"try.csv");
        } catch (Exception e){
            System.out.println("DIDNT WORK");
            e.printStackTrace();
        }

    }

    public static void identity(){

        ParserResult parserResult = new ParserResult(hardPath+"target/surefire-reports/try.csv");

        ParserString2Story parserString2Story = new ParserString2Story(Refactor.ToString());

        parserString2Story.identify(parserResult.getResultsList());

        System.out.println(parserString2Story.getAllowStory());

        // write the whole story to new Story
        File file = new File(hardPath+"src/main/resources/reports/allow.story");

        CreateStory.WirteFile(parserString2Story.getAllowStory(),file);
    }

}