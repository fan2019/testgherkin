package FileAction;

import LoginTest.*;
import org.jbehave.core.embedder.Embedder;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;


import java.io.*;
import java.util.Arrays;

public class Runme {

    private static Embedder embedder = new Embedder();

    public static void main(String[] args) throws Throwable {

        // get the purpose file resource
        Resource newRes = new ClassPathResource("stories/newStory.story");
        File newFile = newRes.getFile();
        // File newFile = new File(res.getFile().getParent()+ "/newStory.story");
        //FileCopyUtils.copy(res.getFile(), newFile);
        Refactor.writeStory(newFile);
        // Get file input
        FileInputStream inputStream = null;
        // Use StringBuilder to recreate the string
        StringBuilder stringBuilder = null;
        try {
            // Read content if it is not empty
            if (newFile.exists()) {
                inputStream = new FileInputStream(newFile);
                InputStreamReader streamReader = new InputStreamReader(inputStream);
                // Create readers
                BufferedReader reader = new BufferedReader(streamReader);
                String lineString = "";
                stringBuilder  = new StringBuilder();
                // Read the first line
                lineString = reader.readLine();
                while (lineString != null) {
                    // Add content to the buffer
                    stringBuilder.append(lineString);
                    // Add a line break
                    stringBuilder.append("\n");
                    // Read the next line
                    lineString = reader.readLine();
                }
                System.err.println(stringBuilder.toString());
                reader.close();
                inputStream.close();

            }else {
                System.err.println("file is empty!");
            }

        } catch (Exception e) {

            e.printStackTrace();

        }

        embedder.embedderControls().doIgnoreFailureInStories(true);
        embedder.candidateSteps().add(new loginSteps());
        embedder.runStoriesAsPaths(Arrays.asList("stories/newStory.story"));

        // write the whole story to new Story
        File file = new File("/Users/yangfan/IdeaProjects/TestGherkin/src/main/resources/reports/allow.story");

        ParserResult parserResult = new ParserResult("/Users/yangfan/IdeaProjects/TestGherkin/target/site/serenity/results.csv");

        ParserString2Story parserString2Story = new ParserString2Story(Refactor.ToString());

        parserString2Story.identify(parserResult.getResultsList());

        CreateStory.WirteFile(parserString2Story.getAllowStory(),file);

    }

}

