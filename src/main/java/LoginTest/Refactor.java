package LoginTest;

import org.apache.commons.lang3.StringUtils;
import org.jbehave.core.embedder.Embedder;
import org.jbehave.core.model.ExamplesTable;
import org.jbehave.core.model.Scenario;
import org.jbehave.core.model.Story;

import java.io.File;
import java.util.*;

public class Refactor {

    private static String RefactorStory;
    private static Embedder embedder = new Embedder();
    private static final String NL = "\n";
    private static ArrayList<String> newCondition = new ArrayList<>();
    private static ArrayList<String> newScenarios = new ArrayList<>();
    private static String GivenString = "";
    private static String ThenString = "";
    private static int indexOfMutation = 0;


    public static String ToString(){

        return RefactorStory;
    }
    
    private static void refactorStory(){
        
        String originalStoryPath = "stories/testStore.story";
        Story story = embedder.storyManager().storyOfPath(originalStoryPath);
        Scenario scenario = story.getScenarios().get(0);
        String title = "Scenario: Mutation Scenarios ";
        List<String> steps = scenario.getSteps();
        ArrayList<String> listOfCondition = new ArrayList<>();
        for (String step : steps) {

            if (step.startsWith("When")) {
                listOfCondition.add(step);
            }

            if (step.startsWith("Given")){
                GivenString = step;
            }

            if (step.startsWith("Then")){
                ThenString = step;
            }
        }

        int size = listOfCondition.size();

        String[] list;

        list = listOfCondition.toArray(new String[size]);

        String table = "Examples:" + NL + scenario.getExamplesTable().asString() + NL;
        Map<String, String> value = new LinkedHashMap<>();
        value.put("result", "true");
        ExamplesTable examplesTable = scenario.getExamplesTable().withRowValues(0, value);

        permutation(list,0);

        //refactor the list make each element to be a scenario
        for (String condition:newCondition) {
            indexOfMutation++;

            String con = StringUtils.strip(condition,"[]");

            if (con.endsWith("button")){
                newScenarios.add(title+indexOfMutation+NL+GivenString+NL+con.replaceAll(", "," \n")+NL+ThenString+NL+table);
            }else{
                String examples = "Examples:";
                newScenarios.add(title+indexOfMutation+NL+GivenString+NL+con.replaceAll(", "," \n")+NL+ThenString+NL+ examples +NL+examplesTable.asString()+NL);
            }
        }

        RefactorStory = StringUtils.strip(newScenarios.toString(),"[]").replaceAll(", ","");

    }
    
    public static void writeStory(File file){

        refactorStory();

        CreateStory.WirteFile(RefactorStory,file);

    }

    private static void permutation(String[] array, int start) {

        if (start == array.length) {
            newCondition.add(Arrays.toString(array));
        } else
            for (int i = start; i < array.length; ++i) {
                swap(array, start, i);
                permutation(array, start + 1);
                swap(array, start, i);
            }
    }

    private static void swap(String[] array, int s, int i) {
        String t = array[s];
        array[s] = array[i];
        array[i] = t;
    }
}